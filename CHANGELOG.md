# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

# [Unrealised]
### Added

### Modified

### Fixed

# [1.0.0]

### Added
  - Added `Extract phrases...`
  - Added `Draw extracted phrases`
  - Added `Organize files`
  - Added `Make report`
  - Added `About`
