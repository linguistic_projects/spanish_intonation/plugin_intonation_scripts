Add menu command: "Objects", "Goodies", "Spanish intonation", "", 0, ""

Add menu command: "Objects", "Goodies", "Extract phrases from stories...", "Spanish intonation", 1, "scripts/extract_phrases_from_stories.praat"
Add menu command: "Objects", "Goodies", "Draw pictures from phrases...", "Spanish intonation", 1, "scripts/draw_extracted_phrases.praat"
Add menu command: "Objects", "Goodies", "Organize files...", "Spanish intonation", 1, "scripts/file_organizer.praat"
Add menu command: "Objects", "Goodies", "Get report as Table...", "Spanish intonation", 1, "scripts/make_report.praat"

Add menu command: "Objects", "Goodies", "-", "Spanish intonation", 1, ""
Add menu command: "Objects", "Goodies", "About", "Spanish intonation", 1, "./scripts/about.praat"