# Intonation scripts plug-in

This is a collection of praat scripts for the analysis of the **Spanish Intonation Database (SID)**. 

## Information

- Homepage
  - https://gitlab.com/linguistic_projects/spanish_intonation/plugin_intonation_scripts
- Version
  - 1.0.0
- Maintainer
  -  Rolando Muñoz Aramburú
- Depends
  - praat: 6.30.0+
- Recommends:
  - License:
    - GPL3 <https://www.gnu.org/licenses/gpl-3.0.html>
- Description:
  - Short:
    - A  collection of praat scripts for the analysis of the **Spanish Intonation Database (SID)**. 
  - Long:
    - A collection of praat scripts for the analysis of the **Spanish Intonation Database (SID)**. This plugin is part of the Spanish Intonation project, lead by professor José Elías-Ulloa (jose.elias-ulloa@stonybrook.edu)
