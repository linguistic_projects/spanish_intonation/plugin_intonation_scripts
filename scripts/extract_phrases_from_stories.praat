# Copyright 2017 Rolando Muñoz

include ../../plugin_intonation_scripts/procedures/get_tier_number.proc
include ../../plugin_intonation_scripts/procedures/remove_isolated_boundary.proc
include ../../plugin_intonation_scripts/procedures/config.proc

@config.init: "../preferences/preferences.txt"

beginPause: "Extract phrases"
  comment: "Input files:"
  comment: "The directory where stories are stored..."
  sentence: "Sound folder", config.init.return$["story_folder_sd"]
  sentence: "Textgrid folder", config.init.return$["story_folder_tg"]
  comment: "The tier name which contains the phrase codes..."
  word: "Tier code", config.init.return$["code_tier"]
  comment: "Output files:"
  comment: "The directory where the resulting files will be stored..."
  sentence: "Save in", config.init.return$["extracted_phrase_folder"]
  comment: "Give a margin(s) to the extracted phrases..."
  real: "Margin", config.init.return$["extracted_phrase_margin"]
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "story_folder_sd", sound_folder$
@config.setField: "story_folder_tg", textgrid_folder$
@config.setField: "extracted_phrase_folder", save_in$
@config.setField: "code_tier", tier_code$
@config.setField: "extracted_phrase_margin", string$(margin)

fileList = Create Strings as file list: "fileList", textgrid_folder$ +"/*.TextGrid"
nFiles = Get number of strings
for iFile to nFiles
  tgName$ = object$[fileList, iFile]
  sdName$ = (tgName$ - "TextGrid") + "wav"
  speaker_code$ = left$(tgName$,3)
  
  if fileReadable(sound_folder$ + "/" + sdName$)
    tg = Read from file: textgrid_folder$ + "/" + tgName$
    sd = Read from file: sound_folder$ + "/" + sdName$
    
    selectObject: tg
    total_duration = Get total duration
    @getTierNumber
    code_tier = getTierNumber.return[tier_code$]
    tb_tg = Down to Table: "no", 16, "yes", "no"
    tb_code = Extract rows where: "self$[""tier""]=""'tier_code$'"""

    nrow = Object_'tb_code'.nrow
    for irow to Object_'tb_code'.nrow
      ## Get phrases properties
      code_tmin = object[tb_code, irow, "tmin"]
      code_tmax = object[tb_code, irow, "tmax"]
      code_text$ = object$[tb_code, irow, "text"]
      ## Extract phrases
      ### From TextGrid
      selectObject: tg
      tg_extracted = Extract part: code_tmin, code_tmax, "yes"

      ### Add margin
      margin_left = if code_tmin <= margin then code_tmin else margin fi
      Extend time: margin_left, "Start"
      margin_right = if (code_tmax+margin) >= total_duration  then total_duration - code_tmax else margin fi
      Extend time: margin_right, "End"
      @remove_isolated_boundary_at_time: 2, code_tmin
      @remove_isolated_boundary_at_time: 2, code_tmax
      @remove_isolated_boundary_at_time: 6, code_tmin
      @remove_isolated_boundary_at_time: 6, code_tmax
      Shift times to: "start time", 0
      ### From Audio
      selectObject: sd
      sd_extracted = Extract part: code_tmin - margin_left, code_tmax + margin_right, "rectangular", 1, "no"

      counter = 0
      repeat
        counter+= 1
        rep$ = if counter < 10 then "R0'counter'" else "R'counter'" fi
        base_name$ = speaker_code$ + "_" + code_text$ + "_" + rep$
      until not fileReadable(save_in$ + "/" + base_name$ + ".TextGrid")
      selectObject: tg_extracted
      Save as text file: save_in$ + "/" + base_name$ + ".TextGrid"
      selectObject: sd_extracted
      Save as WAV file: save_in$ + "/" + base_name$ + ".wav"

      removeObject: tg_extracted, sd_extracted
    endfor
    removeObject: tg, sd, tb_tg, tb_code
  else
    pauseScript: "Warning: missing sound"
  endif
endfor
removeObject: fileList
pauseScript: "Completed successfully"