# Copyright 2017 Rolando Muñoz Aramburú
include ../../plugin_intonation_scripts/procedures/config.proc

@config.init: "../preferences/preferences.txt"

beginPause: "File organizer"
  comment: "Input:"
  comment: "The directory where files are stored..."
  sentence: "Folder_source", ""
  comment: "Output:"
  comment: "The new files will be copied to..."
  sentence: "Folder_destiny", ""
  comment: "Order your files according to the following fields..."
  sentence: "Order by", config.init.return$["file_organizer.order_by"]
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "file_organizer.order_by", order_by$

fileList = Create Strings as file list: "fileList", folder_source$ +"/*"
nFiles = Get number of strings

fieldList = Create Strings as tokens: order_by$, " "
nFields = Get number of strings

tb_speaker = Read Table from tab-separated file: "../preferences/info_speakers.txt"
tb_phrase = Read Table from tab-separated file: "../preferences/info_targets.txt"
tb_tableInfo = Create Table with column names: "table", 0, "table_id field_name"

## Check if exists fields written in order_by$ variable

for i to nFields
  fieldName$= object$[fieldList, i]
  selectObject: tb_tableInfo
  Append row
  
  selectObject: tb_speaker
  fieldColumn = Get column index: fieldName$
  if fieldColumn
    selectObject: tb_tableInfo
    Set numeric value: Object_'tb_tableInfo'.nrow, "table_id", tb_speaker
    Set string value: Object_'tb_tableInfo'.nrow, "field_name", fieldName$
  else
    selectObject: tb_phrase
    fieldColumn = Get column index: fieldName$
    if fieldColumn
      selectObject: tb_tableInfo
      Set numeric value: Object_'tb_tableInfo'.nrow, "table_id", tb_phrase
      Set string value: Object_'tb_tableInfo'.nrow, "field_name", fieldName$
    else
      exitScript: "The """ + fieldName$ + """ field could not be found."  
    endif
  endif
endfor

for iFile to nFiles
  file_name$ = object$[fileList, iFile]
  speaker_code$ = left$(file_name$, 3)
  phrase_code$ = mid$(file_name$, 5, 7)

  ## Check if file_name$ is registered in the tables
  selectObject: tb_speaker
  tb_speaker_row = Search column: "speaker_code", speaker_code$
  selectObject: tb_phrase
  tb_phrase_row = Search column: "phrase_code", phrase_code$
  
  folder_destiny_tmp$ = folder_destiny$ + "/"
  for i to Object_'tb_tableInfo'.nrow
    fieldName$ = object$[tb_tableInfo, i, "field_name"]
    tb_id = object[tb_tableInfo, i, "table_id"]
    row = if tb_id = tb_speaker then tb_speaker_row else tb_phrase_row fi
    fieldValue$ = object$[tb_id, row, fieldName$]
    folder_destiny_tmp$ = folder_destiny_tmp$ + fieldValue$ + "/"
    createDirectory: folder_destiny_tmp$
  endfor
  if not windows
    runSystem: "cp ""'folder_source$'/'file_name$'"" ""'folder_destiny_tmp$'"""
  endif
endfor
removeObject: tb_speaker, tb_phrase, tb_tableInfo, fieldList, fileList
pauseScript: "Completed successfully"