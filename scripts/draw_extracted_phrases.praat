# Copyright 2017 Rolando Muñoz Aramburú

include ../../plugin_intonation_scripts/procedures/local_lib.proc
include ../../plugin_intonation_scripts/procedures/get_tier_number.proc
include ../../plugin_intonation_scripts/procedures/config.proc

@config.init: "../preferences/preferences.txt"

beginPause: "Draw_phrases"
  comment: "Input:"
  comment: "The directory where the extracted phrases are stored..."
  sentence: "Sound folder", config.init.return$["extracted_phrase_folder"]
  sentence: "Textgrid folder", config.init.return$["extracted_phrase_folder"]
 comment: "Tier names..."
  word: "Tier strsyll", config.init.return$["strsyll_tier"]
  word: "Tier words", config.init.return$["word_tier"]
  comment: "Split phrases into chunks if..."
  word: "Split phrases where interval label", "pause"
  real: "If interval label greater than", 0.1
  comment: "Output:"
  comment: "The directory where the phrase pictures will be stored..."
  sentence: "Save in", config.init.return$["picture_folder"]
   comment: "Give a margin(s) to the phrases pictures..."
  real: "Margin", config.init.return$["picture_margin"]
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "extracted_phrase_folder", sound_folder$
@config.setField: "extracted_phrase_folder", textgrid_folder$
@config.setField: "picture_folder", save_in$
@config.setField: "strsyll_tier", tier_strsyll$
@config.setField: "word_tier", tier_words$
@config.setField: "picture_margin", string$(margin)

fileList = Create Strings as file list: "fileList", textgrid_folder$ +"/*.TextGrid"
nFiles = Get number of strings
for iFile to nFiles
  tgName$ = object$ [fileList, iFile]
  sdName$ = (tgName$ - "TextGrid") + "wav"
  
  if not fileReadable(sound_folder$ + "/" + sdName$)
    exitScript: "Warning: missing sound"
  endif
  
  ## Open Sound and TextGrid
  sd = Read from file: sound_folder$ + "/" + sdName$
  tg = Read from file: textgrid_folder$ + "/" + tgName$

  ## Keep necessary tiers
  @getTierNumber
  tg_strsyll = getTierNumber.return[tier_strsyll$]
  tg_words = getTierNumber.return[tier_words$]
  Duplicate tier: tg_words, 1, tier_words$
  Duplicate tier: tg_strsyll+1, 1, tier_strsyll$

  tier_strsyll = 1 
  tier_words = 2
  nTiers = Get number of tiers
  for iTier from 3 to nTiers
    Remove tier: iTier
  endfor
  
  ## Convert pauses < 0.01 to P
  nIntervals = Get number of intervals: tier_words
  for iInterval to nIntervals
    interval_text$ = Get label of interval: tier_words, iInterval
    if interval_text$ == split_phrases_where_interval_label$
      interval_start = Get start time of interval: tier_words, iInterval
      interval_end = Get end time of interval: tier_words, iInterval
      interval_duration = interval_end - interval_start
      if interval_duration < if_interval_label_greater_than
        Set interval text: tier_words, iInterval,"P"
      endif
    endif
  endfor

  ## Run a script which concatenates non_pauses intervals as groups
  runScript: "group_non_empty_intervals.praat", tier_words, split_phrases_where_interval_label$, if_interval_label_greater_than
  tb_phrases = selected("Table")
  for iRow to Object_'tb_phrases'.nrow
    file_name$ = object$[tb_phrases, iRow, "file_name_picture"]
    phrase_tmin = object[tb_phrases, iRow, "tmin"]
    phrase_tmax = object[tb_phrases, iRow, "tmax"]
    
    selectObject: tg
    tg_extracted = Extract part: phrase_tmin, phrase_tmax, "no"
    Extend time: margin, "End"
    Extend time: margin, "Start"
    Shift times to: "start time", 0
    duration= Get total duration

    selectObject: tg_extracted
    tb_tg_extracted= Down to Table: "no", 16, "yes", "no"

    selectObject: sd
    sd_extracted = Extract part: phrase_tmin - margin, phrase_tmax+margin, "rectangular", 1, "no"
    pitch_extracted = noprogress To Pitch: 0, 75, 600
    pitch_extracted_smoothed = Smooth: 10
    
    selectObject: sd_extracted
    spectrogram_extracted = noprogress To Spectrogram: 0.005, 8000, 0.0002, 20, "Gaussian"

    ## Draw

    ## Clear drawing space
    Erase all

    ## Title
    Select outer viewport: 0, 6, 0, 7
    file_name_modified$= replace$(file_name$, "_", "\_ ", 0)
    Viewport text: "Left", "Top", 0, file_name_modified$

    ## Draw TextGrid
    selectObject: tg_extracted
    Select outer viewport: 0, 6, 0, 7
    Draw: 0, 0, "no", "yes", "yes"
    Line width: 0.5
    Draw inner box
    Line width: 1.0
  
    ## Draw Spectrogram
    selectObject: spectrogram_extracted
    Select outer viewport: 0, 6, 2.24, 4.90
    Paint: 0, 0, 0, 0, 100, "yes", 40, 6, 0, "no"
    
    # Draw shadow areas in stressed syllables. Also, trace word and syllable boundaries.
    Select outer viewport: 0, 6, 0, 3.02
    Axes: 0, duration, 0, 1
    
    for annotation_iRow to Object_'tb_tg_extracted'.nrow
      annotation_tier$ = object$[tb_tg_extracted, annotation_iRow, "tier"]
      annotation_tmin = object['tb_tg_extracted',annotation_iRow, "tmax"]
      annotation_tmax = object['tb_tg_extracted',annotation_iRow, "tmin"]
      
      if annotation_tier$ ==  "StrSyll"
        Select outer viewport: 0, 6, 0, 3.02
        Paint rectangle: 0.9, annotation_tmin, annotation_tmax, 0, 1
        Line width: 0.5
      elsif annotation_tier$ = "Words"
        Line width: 1
      endif
      Select outer viewport: 0, 6, 0, 4.90
      Dotted line
      Draw line: annotation_tmin, 0, annotation_tmin, 1
      Draw line: annotation_tmax, 0, annotation_tmax, 1
      Solid line
    endfor
      
    ## Draw Pitch
    selectObject: pitch_extracted_smoothed
    max_pitch = Get maximum: 0, 0, "Hertz", "Parabolic"
    min_pitch = Get minimum: 0, 0, "Hertz", "Parabolic"
    adjusted_max_pitch = round(max_pitch) + 20
    adjusted_min_pitch = round(min_pitch) - 20
    Line width: 2

    Select outer viewport: 0, 6, 0, 3.02
    Draw: 0, 0, adjusted_min_pitch, adjusted_max_pitch, "no"
    Line width: 0.5
    Draw inner box
    One mark left: adjusted_max_pitch, "yes", "yes", "no", ""
    One mark left: adjusted_min_pitch, "yes", "yes", "no", ""
    Marks left every: 1.0, 50, "yes", "yes", "no"
    Text left: "yes", "Pitch (Hz)"
    
    ## Save pictures
    Select outer viewport: 0, 6, 0, 7
    if windows 
      Save as 300-dpi PNG file: save_in$ +"/"+file_name$+".png"
    else
      Save as PDF file: save_in$ +"/"+file_name$+".pdf"
    endif
    ## Remove objects
    removeObject: sd_extracted, tg_extracted, pitch_extracted, pitch_extracted_smoothed, tb_tg_extracted, spectrogram_extracted
  endfor    
  removeObject: sd, tg, tb_phrases
endfor
removeObject: fileList
## Clear drawing space
Erase all
Select outer viewport: 0, 6, 0, 4

pauseScript: "Completed successfully"