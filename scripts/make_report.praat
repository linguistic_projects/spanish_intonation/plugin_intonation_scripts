# Copyright 2017 Rolando Muñoz Aramburú

include ../../plugin_intonation_scripts/procedures/local_lib.proc
include ../../plugin_intonation_scripts/procedures/get_tier_number.proc
include ../../plugin_intonation_scripts/procedures/config.proc

@config.init: "../preferences/preferences.txt"

beginPause: "Make report"
  comment: "Input:"
  comment: "The directory where phrases are stored in..."
  sentence: "Textgrid folder", config.init.return$["extracted_phrase_folder"]
  comment: "The tier name which contains the words..."
  word: "Tier words", config.init.return$["word_tier"]
  comment: "Pause settings..."
  word: "Split phrases where interval label", "pause"
  real: "If interval label greater than", 0.1
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

fileList = Create Strings as file list: "fileList", textgrid_folder$ +"/*.TextGrid"
nFiles = Get number of strings
tb_objects_id# = zero#(nFiles)
for iFile to nFiles
  tgName$ = object$ [fileList, iFile]

  ## Open Sound and TextGrid
  tg = Read from file: textgrid_folder$ + "/" + tgName$
  @getTierNumber
  tier_words = getTierNumber.return[tier_words$]

  ## Run a script which concatenates non_pauses intervals as groups
  runScript: "group_non_empty_intervals.praat", tier_words, split_phrases_where_interval_label$, if_interval_label_greater_than
  tb_objects_id# [iFile] = selected("Table")
  removeObject: tg
endfor

if variableExists("tb_objects_id#")
  selectObject: tb_objects_id#[1]
  for i from 2 to nFiles
    plusObject: tb_objects_id#[i]
  endfor
  tb = Append
  for i to nFiles
    removeObject: tb_objects_id#[i]
  endfor
endif

removeObject: fileList
selectObject: tb
Rename: "Report"
pauseScript: "Completed successfully"