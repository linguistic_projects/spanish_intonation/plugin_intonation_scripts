# Copyright 2017 Rolando Muñoz Aramburú

form Group non-empty intervals 
  natural Tier 3
  word Split_where_interval_label pause
  real If_interval_label_greater_than 0.1
endform

tg = selected("TextGrid")
tg_name$ = selected$("TextGrid")

tg_tier = Extract one tier: tier
tb_tier = Down to Table: "no", 16, "yes", "no"

alphabet = Read Strings from raw text file: "../../plugin_intonation_scripts/preferences/alphabet.txt"
tb = Create Table with column names: "phrases", 0, "speaker_code phrase_code phrase_text tmin tmax file_name file_name_picture part"

break = 0
phrase$ = ""

for i to Object_'tb_tier'.nrow
  text$ = object$[tb_tier, i, "text"]
  tmin = object[tb_tier, i, "tmin"]
  tmax = object[tb_tier, i, "tmax"]
  if text$ == split_where_interval_label$ and break
    dur = tmax - tmin
    if dur > if_interval_label_greater_than
      @_add2Table
      break = 0
      phrase$ = ""
    endif
  elsif text$ <> split_where_interval_label$
    break += 1
    phrase$ = phrase$ + text$ + " "
    if break = 1
      tmin_chunk = tmin
      tmax_chunk = tmax
    else
      tmax_chunk = tmax
    endif
  endif
endfor

if text$ <> split_where_interval_label$
  @_add2Table
endif

removeObject: tg_tier, tb_tier, alphabet

selectObject: tb
Formula: "file_name_picture", "if Object_'tb'.nrow > 1 then self$[row, ""file_name""] + ""_"" + self$[""part""] else self$[row, ""file_name""] fi"
Formula: "speaker_code", "mid$(self$[row, ""file_name""], 1, 3)"
Formula: "phrase_code", "mid$(self$[row, ""file_name""], 5, 7)"

procedure _add2Table
  Append row
  Set string value: Object_'tb'.nrow, "file_name", tg_name$
  Set string value: Object_'tb'.nrow, "part", Object_'alphabet'$[Object_'tb'.nrow]
  Set string value: Object_'tb'.nrow, "phrase_text", phrase$ - " "
  Set numeric value: Object_'tb'.nrow, "tmin", tmin_chunk
  Set numeric value: Object_'tb'.nrow, "tmax", tmax_chunk
endproc